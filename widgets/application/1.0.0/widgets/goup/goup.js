/*global
YAHOO
*/

(function () {
    YAHOO.namespace("ttao");
    var ttao = YAHOO.ttao;
    var Toronto = YAHOO.com.aviarc.framework.toronto;

    ttao.goup = function() {
        
    };

    YAHOO.lang.extend(ttao.goup, Toronto.framework.DefaultActionImpl, {
        // The 'startup' method may be deleted if it is not required, the method from DefaultWidgetImpl will be used
        // Removing the superclass.startup method call may prevent your widget from functioning
        startup: function () {

        },

        run: function (state) {

            var startdir = this.getAttribute("startdir",state);

            console.log(startdir);

            console.log(startdir.lastIndexOf("/"));
            
            curdir = startdir.substring(0,startdir.lastIndexOf("/"));
            upperdir = curdir.substring(0,curdir.lastIndexOf("/"));
            
            console.log("curdir " + curdir);
            console.log("upperdir " + upperdir);
            
            state.getExecutionState().setReturnValue(upperdir);
        }
    });
})();
