package newapp.xmlcommands;

import com.aviarc.core.state.State;
import com.aviarc.framework.xml.command.AbstractAutoAttributeXMLCommand;
import com.aviarc.core.dataset.Dataset;
import com.aviarc.core.dataset.DatasetRow;
import com.aviarc.core.logging.AviarcLogger;
import com.aviarc.core.logging.LoggingHub;
import java.io.File;

public class GetDirectoryListing extends AbstractAutoAttributeXMLCommand {
    private static final long serialVersionUID = 0L;
    private static final AviarcLogger logger = LoggingHub.getGeneralLogger();

    @Override
    public void perform(State s) {
        // Attributes
        String directory = getAttributeValue("directory");
        String aDatasetName = getAttributeValue("dataset");
        String prefixForName = getAttributeValue("prefix-for-name");
        
        
        Dataset aDataset = s.getApplicationState().getDatasetStack().findDataset(aDatasetName);
        DatasetRow aRow;
        
        File dir = new File(directory);
        File temp;
        File temppic;
        
        logger.info("######## Lets go! " + directory);
        
        
        String[] children = dir.list();
        
        if (children !=null) {
            for (int i=0; i<children.length; i++) {
                aRow = aDataset.createRow();            
                aRow.setField("basename",children[i]);
                aRow.setField("name",prefixForName + "/" + children[i]);

                temp = new File(directory + "/" + children[i]);
                
                logger.info("        ######## is this a directory " + temp.getName());
                logger.info("        ######## directory " + temp.isDirectory());
                logger.info("        ######## file " + temp.isFile());
                
                if(temp.isDirectory()){
                    aRow.setField("type","directory");
                    aRow.setField("picture",prefixForName + "/" + children[i] + "/cover.jpg");
                    
                    temppic  = new File(directory + "/" + children[i] + "/cover.jpg");
                    
                    logger.info("        ######## yep " + temp.getName());
                } else {
                    logger.info("        ######## nope " + temp.getName());
                    aRow.setField("type","file");
                    
                    aRow.setField("picture",prefixForName + "/cover.jpg");
                    
                    temppic  = new File(directory + "/cover.jpg");
                }
                
                logger.info("        ######## temppic " + temppic);

                
                if(!temppic.isFile()){
                    aRow.setField("picture","www/default.png");
                }
                
            }
        }

        
    }

}
